def christmas(text):
    return ("""                                                        \033[33;5m*
     \033[33m*                                                          *
                                  *                  *\033[0m        \033[31m.--.\033[0m
      \/ \/  \/  \/                                        \033[31m./   /=\033[0m*
        \/     \/  \033[33;5m    *            *   \033[0m             ...  \033[31m(_____)\033[0m
         \ ^ ^/                                       \033[31m\ \_\033[0m((\033[34m^\033[31mo\033[0m\033[34m^\033[0m))\033[31m-.\033[0m    \033[33;5m*\033[0m
         (\033[34mo\033[0m)(\033[34mO\033[0m)--)--------\.                           \033[31m\ \033[0m  (   ) \033[31m\ \ \033[0m\033[35m._.\033[0m
         |    |  \033[33m||================\033[36m((~~~~~~~~~~~~~~~~~))\033[31m| \033[0m  ( ) \033[31m  |\033[0m  \033[35m  \ \033[0m
          \__/             ,|      \033[36m  \. \033[33m* * * * * *\033[0m \033[36m./  \033[31m(~~~~~~~~~~)\033[0m  \033[35m  \ \033[0m
 \033[33;5m  *  \033[0m      ||^||\.____./|| |        \033[36m  \___________/     \033[31m~||~~~~|~'\033[0m\033[35m\____/\033[0m \033[33;5m*\033[0m
            || ||     || || A         \033[36m   ||    ||         \033[31m||    |\033[0m   \033[m %s
   \033[33;5m  * \033[0m     <> <>     <> <>        \033[36m  (___||____||_____)  \033[31m((~~~~~|\033[33;5m   *\033[0m """ % text)


def winter(text):
    return ("""    \033[0;5m*\033[0m ,\033[32m_\033[0m o    \033[0;5m*\033[0m       ,\033[35m_\033[0m o    \033[0;5m*\033[0m
     \033[33m/ \033[32m//\\\033[0m,       \033[0;5m*\033[0m  \033[33m/ \033[35m//\\\033[0m, \033[0;5m*\033[0m
 \033[0;5m*\033[0m    \\\033[32m>> \033[33m|\033[0m  \033[0;5m*\033[0m    \033[0;5m*  \033[0m \\\033[35m>> \033[33m|\033[0m   %s
     \033[0;5m* \033[0m\\\,      \033[0;5m*\033[0m      \\\,    \033[0;5m*\033[0m """ % text)


def summer(text):
    return ("""\033[32m_\/_               \033[33;5m  |              \033[0;32m  _\/_\033[0m
\033[32m/\033[31mo\033[32m\\\ \033[0m        \033[33;5m    \       /         \033[0;32m   //\033[31mo\033[32m\ \033[0m
\033[30m |               \033[33m  .---.         \033[30m       | \033[0m %s
\033[36m_\033[30m|\033[36m_______   \033[33;5m  -- \033[0;33m /     \ \033[33;5m --  \033[0;36m   ______\033[30m|\033[36m__\033[0m
        \033[34m \`~^~^~^~^~^~^~^~^~^~^~^~\`\033[0m """ % text)
