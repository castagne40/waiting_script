Flask==1.1.1
nose==1.3.7
numpy==1.17.4
requests==2.22.0
gunicorn==20.0.4
