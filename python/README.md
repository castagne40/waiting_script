# Python

## To launch tests

```
nosetests test_waiting.py
```

## Localhost GUI

### To launch

```
python3 waiting_gui.py
```

### To test API

```
curl -i http://127.0.0.1:5000/api -X POST --data 'date=2019-12-24 23:59&text=&days_off=&absence=0'
```

## Heroku

### Init

```
heroku login
heroku git:remote -a app-waiting
```

### Deploy

```
git add .
git commit -m "Commit"
git push heroku master
```
