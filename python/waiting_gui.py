import waiting
import argparse
import json
from flask import Flask, render_template, redirect, request, make_response


app = Flask(__name__)


@app.route('/')
def call_waiting():
    req_args = request.args
    cookie_args = request.cookies.get('waiting_content')
    if req_args or cookie_args:
        if req_args:
            new_args = json.loads(req_args.get('waiting_content'))
        else:
            new_args = json.loads(cookie_args)
        args = argparse.Namespace()
        args.date = new_args['date']
        args.absence = int(new_args['absence'])
        args.days_off = new_args['days_off']
        args.text = new_args['text']
        args.today = ''
        args.version = False
        args.verbose = False
        args.christmas = False
        args.summer = False
        args.winter = False
        try:
            new_waiting = waiting.main(args)
            return render_template("index.html", display=new_waiting, version=waiting.get_version())
        except Exception as e:
            return render_template("error.html", error=str(e))
    else:
        return render_template("index.html", display="", version=waiting.get_version())


@app.route('/setup')
def setup_waiting():
    new_args = request.cookies.get('waiting_content')
    if new_args:
        new_args = json.loads(new_args)
    return render_template("setup.html", version=waiting.get_version(), args=new_args)


@app.route('/result', methods=['POST'])
def result_waiting():
    if request.method == 'POST':
        req_args = json.dumps(request.form)
        cookie_args = json.dumps(request.form.to_dict())
        response = make_response(redirect('/?waiting_content=' + req_args))
        response.set_cookie("waiting_content", cookie_args)
        return response


@app.route('/api', methods=['POST'])
def api_waiting():
    if request.method == 'POST':
        req_args = request.form.to_dict()
        if req_args:
            new_args = req_args
            args = argparse.Namespace()
            args.date = new_args['date']
            args.absence = int(new_args['absence'])
            args.days_off = new_args['days_off']
            args.text = new_args['text']
            args.today = ''
            args.version = False
            args.verbose = False
            args.christmas = False
            args.summer = False
            args.winter = False
            try:
                new_waiting = waiting.main(args).split(' ')[-1]
                json_waiting = '{"text": "' + args.text + '", "remaining_days": "' + new_waiting + '"}'
                return json.loads(json_waiting)
            except Exception as e:
                return str(e)
        else:
            return "no args"


if __name__ == "__main__":
    app.run()
