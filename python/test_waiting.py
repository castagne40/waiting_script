import waiting
import argparse
import unittest
import sys
import io
import datetime


def call_waiting(date="", today="", days_off="", absence=0, version=False, verbose=False,
                 text="", summer=False, winter=False, christmas=False):
    args = argparse.Namespace()
    args.absence = absence
    args.date = date
    args.today = today
    args.text = text
    args.verbose = verbose
    args.version = version
    args.summer = summer
    args.christmas = christmas
    args.winter = winter
    args.days_off = days_off

    return waiting.main(args)


class Tests(unittest.TestCase):
    def test_import_works(self):
        # test of waiting.main()
        self.assertIn("Holiday", call_waiting(date="2030-01-31 17:00"))

    def test_invalid_date(self):
        # test with an invalid date
        with self.assertRaises(ValueError) as cm:
            call_waiting(date="20300131 17:00")
        self.assertEqual(str(cm.exception), "time data '20300131 17:00' does not match format '%Y-%m-%d %H:%M'")

    def test_valid_stdout(self):
        # test simple stdout
        saved_stdout = sys.stdout
        try:
            out = io.StringIO()
            sys.stdout = out
            call_waiting(date="2030-01-31 17:00")
            output = out.getvalue().strip()
            assert "Holiday : D-" in output
        finally:
            sys.stdout = saved_stdout

    def test_valid_day_with_weekend(self):
        # test of displaying the remaining days
        saved_stdout = sys.stdout
        tomorrow = datetime.datetime.today() + datetime.timedelta(days=7)
        try:
            out = io.StringIO()
            sys.stdout = out
            call_waiting(date=tomorrow.strftime('%Y-%m-%d %H:%M'))
            output = out.getvalue().strip()
            assert "D-5" in output
        finally:
            sys.stdout = saved_stdout

    def test_valid_day_with_weekend_and_one_absence(self):
        # test of displaying the remaining days with one absence
        saved_stdout = sys.stdout
        tomorrow = datetime.datetime.today() + datetime.timedelta(days=7)
        try:
            out = io.StringIO()
            sys.stdout = out
            call_waiting(tomorrow.strftime('%Y-%m-%d %H:%M'), absence=1)
            output = out.getvalue().strip()
            assert "D-4" in output
        finally:
            sys.stdout = saved_stdout

    def test_invalid_absence(self):
        # test with an invalid today date
        today_date = "2019-12-05 11:00"
        end_date = "2019-12-06 17:00"
        with self.assertRaises(ValueError) as cm:
            call_waiting(date=end_date, today=today_date, absence=-1)
        self.assertEqual(str(cm.exception), "ERROR : number of absences not greater than 0.")

    def test_valid_day_with_custom_today_date(self):
        # test of displaying the remaining days with custom today date
        saved_stdout = sys.stdout
        today_date = "2019-12-05 11:30"
        end_date = "2019-12-06 17:00"
        try:
            out = io.StringIO()
            sys.stdout = out
            call_waiting(date=end_date, today=today_date)
            output = out.getvalue().strip()
            assert "D-1" in output
        finally:
            sys.stdout = saved_stdout

    def test_valid_hours_with_custom_today_date(self):
        # test of displaying the remaining hours with custom today date
        saved_stdout = sys.stdout
        today_date = "2019-12-06 11:00"
        end_date = "2019-12-06 17:00"
        try:
            out = io.StringIO()
            sys.stdout = out
            call_waiting(date=end_date, today=today_date)
            output = out.getvalue().strip()
            assert "H-6" in output
        finally:
            sys.stdout = saved_stdout

    def test_error_absence_greater_than_days_left(self):
        # test of displaying error when absence are greater than days left
        today_date = "2019-12-05 11:00"
        end_date = "2019-12-06 17:00"
        with self.assertRaises(ValueError) as cm:
            call_waiting(date=end_date, today=today_date, absence=2)
        self.assertEqual(str(cm.exception), "ERROR : Date older than today OR absence greater than days left.")

    def test_invalid_today_date(self):
        # test with an invalid today date
        today_date = "20191205 11:00"
        end_date = "2019-12-06 17:00"
        with self.assertRaises(ValueError) as cm:
            call_waiting(date=end_date, today=today_date)
        self.assertEqual(str(cm.exception), "time data '20191205 11:00' does not match format '%Y-%m-%d %H:%M'")

    def test_valid_day_with_absence_date(self):
        # test of displaying the remaining days with custom today date and absence date
        saved_stdout = sys.stdout
        today_date = "2019-12-02 11:30"
        absence_date = "2019-12-03"
        end_date = "2019-12-06 17:00"
        try:
            out = io.StringIO()
            sys.stdout = out
            call_waiting(date=end_date, today=today_date, days_off=absence_date)
            output = out.getvalue().strip()
            assert "D-3" in output
        finally:
            sys.stdout = saved_stdout

    def test_valid_day_with_multiple_absence_dates(self):
        # test of displaying the remaining days with custom today date and multiple absence dates
        saved_stdout = sys.stdout
        today_date = "2019-12-02 11:30"
        absence_date = "2019-12-03,2019-12-04"
        end_date = "2019-12-06 17:00"
        try:
            out = io.StringIO()
            sys.stdout = out
            call_waiting(date=end_date, today=today_date, days_off=absence_date)
            output = out.getvalue().strip()
            assert "D-2" in output
        finally:
            sys.stdout = saved_stdout

    def test_invalid_absence_date(self):
        # test with an invalid today date
        today_date = "2019-12-02 11:30"
        absence_date = "20191203"
        end_date = "2019-12-06 17:00"
        with self.assertRaises(ValueError) as cm:
            call_waiting(date=end_date, today=today_date, days_off=absence_date)
        self.assertEqual(str(cm.exception), "time data '20191203' does not match format '%Y-%m-%d'")

    def test_valid_day_with_custom_text(self):
        # test of displaying a custom text
        saved_stdout = sys.stdout
        try:
            out = io.StringIO()
            sys.stdout = out
            call_waiting(date="2030-01-31 17:00", text="Ciao :")
            output = out.getvalue().strip()
            assert "Ciao : D-" in output
        finally:
            sys.stdout = saved_stdout

    def test_version(self):
        # test of displaying version
        saved_stdout = sys.stdout
        try:
            out = io.StringIO()
            sys.stdout = out
            with self.assertRaises(SystemExit):
                call_waiting(version=True)
            output = out.getvalue().strip()
            assert "waiting.py 1.1" in output
        finally:
            sys.stdout = saved_stdout

    def test_not_date(self):
        # test of displaying error when date is not filled
        with self.assertRaises(ValueError) as cm:
            call_waiting()
        self.assertEqual(str(cm.exception), "ERROR: the following arguments are required: -d/--date")

    def test_verbose(self):
        # test of verbose mode
        saved_stdout = sys.stdout
        try:
            out = io.StringIO()
            sys.stdout = out
            call_waiting(date="2030-01-31 17:00", verbose=True)
            output = out.getvalue().strip()
            assert "Enabling classic mode" in output
        finally:
            sys.stdout = saved_stdout

    def test_mode(self):
        # test of summer mode (not christmas and winter)
        saved_stdout = sys.stdout
        try:
            out = io.StringIO()
            sys.stdout = out
            call_waiting(date="2030-01-31 17:00", summer=True)
            output = out.getvalue().strip()
            assert "~^~^~^~^~^~^~^~^~^~^~^~" in output
        finally:
            sys.stdout = saved_stdout


if __name__ == "__main__":
    unittest.main()
