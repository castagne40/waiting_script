import sys
import argparse
import datetime
import numpy
import display_mode
import os


version = '1.1'


def workdays_left(date, today):
    _today = today.strftime('%Y-%m-%d')
    _end = date.strftime('%Y-%m-%d')
    _days = numpy.busday_count(_today, _end)
    return int(_days)


def hours_left(date, today):
    _today = int(today.strftime('%H'))
    _end = int(date.strftime('%H'))
    _hours = _end - _today
    return int(_hours)


def absence_left(date, today, days_off, absence):
    for day in days_off.split(','):
        _day_off = datetime.datetime.strptime(day, '%Y-%m-%d')
        if today < _day_off < date:
            _day_off = _day_off.strftime('%Y-%m-%d')
            if numpy.is_busday(_day_off):
                absence += 1
    return int(absence)


def display(date, type_date, text):
    if type_date == 'day':
        return "%s D-%s" % (text, date)
    elif type_date == 'hour':
        return "%s H-%s" % (text, date)


def get_version():
    return version


def main(args):
    if args.version:
        print("%s %s" % (os.path.basename(__file__), get_version()))

    if args.date:
        date = datetime.datetime.strptime(args.date, '%Y-%m-%d %H:%M')
    elif not args.date and args.version:
        sys.exit(0)
    elif not args.date:
        raise ValueError("ERROR: the following arguments are required: -d/--date")

    if args.today:
        today = datetime.datetime.strptime(args.today, '%Y-%m-%d %H:%M')
    else:
        today = datetime.datetime.today()

    if args.absence:
        if args.absence >= 0:
            absence = args.absence
        else:
            raise ValueError("ERROR : number of absences not greater than 0.")
    else:
        absence = 0

    if args.text:
        text = args.text
    else:
        text = 'Holiday :'

    days = workdays_left(date, today)

    if args.days_off:
        absence = absence_left(date, today, args.days_off, absence)

    days -= absence

    if days == 0:
        type_date = 'hour'
        left = hours_left(date, today)
    elif days > 0:
        type_date = 'day'
        left = days
    else:
        raise ValueError("ERROR : Date older than today OR absence greater than days left.")

    if args.christmas:
        mode = 'christmas'
    elif args.winter:
        mode = 'winter'
    elif args.summer:
        mode = 'summer'
    else:
        mode = 'classic'

    if args.verbose:
        print("Enabling %s mode" % mode)
        print("Date : %s" % args.date)
        print("Number of absences : %s" % absence)
        print("Remaining : %s" % left)
        print("Text : %s" % text)
        print("Version : %s" % get_version())

    text_left = display(left, type_date, text)

    if mode == 'classic':
        print(text_left)
    elif mode == 'christmas':
        print(display_mode.christmas(text_left))
    elif mode == 'winter':
        print(display_mode.winter(text_left))
    elif mode == 'summer':
        print(display_mode.summer(text_left))

    return text_left


if __name__ == "__main__":
    description = "Count of working days before departure on vacation or other"

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("-d", "--date", help="set date of departure (Y-m-d H:M:S) REQUIRED")
    parser.add_argument("-T", "--today", help="set date of today (Y-m-d H:M:S) if you need for tests")
    parser.add_argument("-a", "--absence", help="set number of absence before departure", type=int)
    parser.add_argument("-D", "--days_off", help="set dates of absence ([Y-m-d, Y-m-d, etc.])")
    parser.add_argument("-x", "--christmas", help="enable christmas mode", action="store_true")
    parser.add_argument("-w", "--winter", help="enable winter mode", action="store_true")
    parser.add_argument("-s", "--summer", help="enable summer mode", action="store_true")
    parser.add_argument("-c", "--classic", help="enable classic mode (default)", action="store_true")
    parser.add_argument("-t", "--text", help="set custom text")
    parser.add_argument("-v", "--verbose", help="verbose", action="store_true")
    parser.add_argument("--version", help="show program version", action="store_true")

    main(parser.parse_args())
