# Waiting script

Count of working days before departure on vacation or other.

2 languages : bash and python.

## Python

### Init

```
virtualenv -p /usr/bin/python3 venv
pip install -r python/requirement.txt
```

### Script

```
usage: python waiting.py [-h] [-d DATE] [-T TODAY] [-a ABSENCE] [-x] [-w] [-s] [-c]
                  [-t TEXT] [-v] [--version]

Count of working days before departure on vacation or other

optional arguments:
  -h, --help                      show this help message and exit
  -d DATE, --date DATE            set date of departure (Y-m-d H:M:S) REQUIRED
  -T TODAY, --today TODAY         set date of today (Y-m-d H:M:S) if you need for tests
  -a ABSENCE, --absence ABSENCE   set number of absence before departure
  -x, --christmas                 enable christmas mode
  -w, --winter                    enable winter mode
  -s, --summer                    enable summer mode
  -c, --classic                   enable classic mode (default)
  -t TEXT, --text TEXT            set custom text just for classic mode
  -v, --verbose                   verbose
  --version                       show program version
```

### Graphic User Interface

#### To execute

```
python python/waiting_gui.py
```

#### To view

Go to http://localhost:5000/

## Bash

```
usage : bash waiting.sh -d DATE [ -a ABSENCE ] [ -x | -w | -s ] [ -t TEXT ] [-h]

Count of working days before departure on vacation or other

optional arguments:
  -h           show this help message and exit
  -d DATE      set date of departure (Ymd H:M:S) REQUIRED
  -a ABSENCE   set number of absence before departure
  -x           enable christmas mode
  -w           enable winter mode
  -s           enable summer mode
  -t TEXT      set custom text just for classic mode
```
