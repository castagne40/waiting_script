#!/bin/bash

####
# Count of working days before departure on vacation or other
####

end='20191111 16:00'
more_absence='0'
type_function='classic'
text='Holidays :'

usage() {
  echo "Usage : $(basename $0) -d DATE [ -a ABSENCE ] [ -x | -w | -s ] [ -t TEXT ] [-h]" >&2
  echo "" >&2
  echo "Count of working days before departure on vacation or other" >&2
  echo "" >&2
  echo "optional arguments:" >&2
  echo "  -h           show this help message and exit" >&2
  echo "  -d DATE      set date of departure (Ymd H:M:S) REQUIRED" >&2
  echo "  -a ABSENCE   set number of absence before departure" >&2
  echo "  -x           enable christmas mode" >&2
  echo "  -w           enable winter mode" >&2
  echo "  -s           enable summer mode" >&2
  echo "  -t TEXT      set custom text just for classic mode" >&2
  exit 1
}

while getopts d:a:xwst:y:h option
do
  case $option in
    h)
      usage
      ;;
    d)
      end=$OPTARG
      ;;
    a)
      more_absence=$OPTARG
      ;;
    x)
      type_function='christmas'
      ;;
    w)
      type_function='snow'
      ;;
    s)
      type_function='summer'
      ;;
    t)
      text=$OPTARG
      ;;
    *)
      usage
      ;;
  esac
done

####
# functions
####

year_today=$((10#$(date +%y)))
day_today=$((10#$(date +%j)))
hour_today=$(($(date +%k)))

remaining_days()
{
  day=$((10#$(date -d "$1" +%j)))
  year=$((10#$(date -d "$1" +%y)))
  extra=$((10#$(date +%w)-1))
  remaining=$((day - day_today + 365*(year-year_today)))
  weekends=$(((remaining+extra)/7))
  result=$((remaining-weekends*2-more_absence))
  echo $result
}

display()
{

  if [ $2 -lt 0 ]
  then
    echo "ERROR"
  elif [ $2 -eq 0 ]
  then
    echo "$1 H-$3 "
  else
    echo "$1 D-$2 "
  fi
}

classic()
{
  classic=$(display "$text" $1 $2)
  echo $classic
}

christmas()
{
  echo -e "                                                        \033[33;5m*"
  echo -e "     \033[33m*                                                          *"
  echo -e "                                  *                  *\033[0m        \033[31m.--.\033[0m"
  echo -e "      \/ \/  \/  \/                                        \033[31m./   /=\033[0m*"
  echo -e "        \/     \/  \033[33;5m    *            *   \033[0m             ...  \033[31m(_____)\033[0m"
  echo -e "         \ ^ ^/                                       \033[31m\ \_\033[0m((\033[34m^\033[31mo\033[0m\033[34m^\033[0m))\033[31m-.\033[0m    \033[33;5m*\033[0m"
  echo -e "         (\033[34mo\033[0m)(\033[34mO\033[0m)--)--------\.                           \033[31m\ \033[0m  (   ) \033[31m\ \ \033[0m\033[35m._.\033[0m"
  echo -e '         |    |  \033[33m||================\033[36m((~~~~~~~~~~~~~~~~~))\033[31m| \033[0m  ( ) \033[31m  |\033[0m  \033[35m  \ \033[0m'
  echo -e '          \__/             ,|      \033[36m  \. \033[33m* * * * * *\033[0m \033[36m./  \033[31m(~~~~~~~~~~)\033[0m  \033[35m  \ \033[0m'
  echo -e " \033[33;5m  *  \033[0m      ||^||\.____./|| |        \033[36m  \___________/     \033[31m~||~~~~|~'\033[0m\033[35m\____/\033[0m \033[33;5m*\033[0m"
  echo -e "            || ||     || || A         \033[36m   ||    ||         \033[31m||    |\033[0m   \033[m$(display 'Christmas :' $1 $2)"
  echo -e "   \033[33;5m  * \033[0m     <> <>     <> <>        \033[36m  (___||____||_____)  \033[31m((~~~~~|\033[33;5m   *\033[0m"
}

snow()
{
  echo -e "    \033[0;5m*\033[0m ,\033[32m_\033[0m o    \033[0;5m*\033[0m        ,\033[35m_\033[0m o    \033[0;5m*\033[0m"
  echo -e "     \033[33m/ \033[32m//\\\\\033[0m,    Ski \033[0;5m*\033[0m  \033[33m/ \033[35m//\\\\\033[0m, \033[0;5m*\033[0m   "
  echo -e " \033[0;5m*\033[0m    \\\\\033[32m>> \033[33m|\033[0m  \033[0;5m*\033[0m$(display '' $1 $2)  \033[0;5m*\033[0m \\\\\033[35m>> \033[33m|\033[0m     "
  echo -e "      \033[0;5m*\033[0m\\\\\,      \033[0;5m*\033[0m       \\\\\,    \033[0;5m*\033[0m"
}

summer()
{
  echo -e "\033[32m_\/_               \033[33;5m  |              \033[0;32m  _\/_\033[0m"
  echo -e "\033[32m/\033[31mo\033[32m\\\\\ \033[0m $(display '' $1 $2)   \033[33;5m  \       /         \033[0;32m   //\033[31mo\033[32m\ \033[0m"
  echo -e "\033[30m |               \033[33m  .---.         \033[30m       | \033[0m"
  echo -e "\033[36m_\033[30m|\033[36m_______   \033[33;5m  -- \033[0;33m /     \ \033[33;5m --  \033[0;36m   ______\033[30m|\033[36m__\033[0m"
  echo -e "        \033[34m \`~^~^~^~^~^~^~^~^~^~^~^~\`\033[0m"
}

####
# calculation
####

days_before_end=$(remaining_days $end)
hour_end=$(($(date -d "$end" +%k)))
hours_before_end=$(($hour_end-$hour_today))

####
# display
####

$type_function $days_before_end $hours_before_end
